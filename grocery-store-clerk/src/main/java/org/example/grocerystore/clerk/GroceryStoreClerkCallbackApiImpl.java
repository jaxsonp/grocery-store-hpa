package org.example.grocerystore.clerk;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.grocerystore.common.api.GroceryStoreClerkCallbackApi;
import org.example.grocerystore.common.model.Customer;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Slf4j
public class GroceryStoreClerkCallbackApiImpl implements GroceryStoreClerkCallbackApi {
    private final ClerkWorker clerkWorker;

    @Override
    public ResponseEntity<Void> customer(Customer customer) {
        log.debug("Received customer {}", customer);
        clerkWorker.checkoutCustomer(customer);
        return ResponseEntity.ok().build();
    }
}

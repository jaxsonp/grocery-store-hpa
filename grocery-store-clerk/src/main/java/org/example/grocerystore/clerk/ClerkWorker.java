package org.example.grocerystore.clerk;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.grocerystore.clerk.config.ClerkProps;
import org.example.grocerystore.common.model.Clerk;
import org.example.grocerystore.common.model.Customer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.net.Inet4Address;
import java.net.UnknownHostException;

@Component
@RequiredArgsConstructor
@Slf4j
public class ClerkWorker {

    private final MetricsServer metricsServer;
    private final ClerkProps clerkProps;
    @Value("${server.port}")
    int port;

    @EventListener(ApplicationReadyEvent.class)
    public void iAmAvailable() throws UnknownHostException {
        String baseUrl = Inet4Address.getLocalHost().getHostAddress();
        log.debug("Letting store know that we are available");
        WebClient webClient = WebClient.create(clerkProps.getStoreBaseUrl());
        webClient.post()
                .uri("/clerk/available")
                .bodyValue(Clerk.builder()
                        .baseUrl(baseUrl + ":" + port)
                        .build())
                .retrieve()
                .toBodilessEntity()
                .subscribe();
    }

    public void checkoutCustomer(Customer customer) {
        log.debug("Checking out customer {}", customer);
        double time = metricsServer.getCheckoutSeconds().time(() -> {
            synchronized (metricsServer.getCheckoutSeconds()){
                try {
                    metricsServer.getCheckoutSeconds().wait(clerkProps.getCheckoutTimeSec() * 1000L);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            log.debug("Done checking out customer {}", customer);
            try {
                iAmAvailable();
            } catch (UnknownHostException ignored) {}
        });
        metricsServer.getCheckoutSeconds().observe(time);
    }

}

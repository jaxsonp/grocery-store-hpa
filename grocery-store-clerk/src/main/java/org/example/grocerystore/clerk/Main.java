package org.example.grocerystore.clerk;

import org.example.grocerystore.clerk.config.ClerkProps;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(ClerkProps.class)
public class Main {

    public static void main(String[] args){
        SpringApplication.run(Main.class, args);
    }

}

package org.example.grocerystore.clerk;

import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.Histogram;
import io.prometheus.client.exporter.common.TextFormat;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.StringWriter;
import java.io.Writer;

@RestController
@Getter
@Slf4j
public class MetricsServer {

    private final Histogram checkoutSeconds = Histogram.build()
            .name("checkout_seconds")
            .help("checkout_seconds Duration of a checking a customer out, in seconds")
            .register(CollectorRegistry.defaultRegistry);

    @GetMapping(value = "/metrics", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> getMetric(){
        log.info("Metrics endpoint called");
        Writer write = new StringWriter();
        try {
            TextFormat.write004(write, CollectorRegistry.defaultRegistry.metricFamilySamples());
        } catch (Exception ignored) {}
        return ResponseEntity.ok(write.toString());
    }
}

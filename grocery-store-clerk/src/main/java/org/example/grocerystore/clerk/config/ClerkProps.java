package org.example.grocerystore.clerk.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@ConfigurationProperties(prefix = "clerk")
@Configuration
public class ClerkProps {

    private String storeBaseUrl;
    private int checkoutTimeSec = 10;

}

package org.example.grocerystore.common.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

public interface GroceryStoreApi {
    public static final String PATH = "/store";

    @PostMapping(path = PATH + "/open")
    ResponseEntity<Void> open();

    @PostMapping(path = PATH + "/addCustomers")
    ResponseEntity<Void> addCustomers(@RequestParam(name = "num") Integer num);

    @PostMapping(path = PATH + "/customerFreq")
    ResponseEntity<Void> customerFreq(@RequestParam(name = "amount", required = false) Integer amount,
                                      @RequestParam(name = "seconds", required = false) Integer seconds);
}

package org.example.grocerystore.common.api;

import org.example.grocerystore.common.model.Clerk;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface GroceryStoreClerkApi {
    public static final String PATH = "/clerk";

    @PostMapping(path = PATH + "/available", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Void> available(@RequestBody Clerk clerk);
}

package org.example.grocerystore.common.api;

import org.example.grocerystore.common.model.Customer;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface GroceryStoreClerkCallbackApi {
    public static final String PATH = "/clerk-callback";

    @PostMapping(path = PATH + "/customer", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Void> customer(@RequestBody Customer customer);
}

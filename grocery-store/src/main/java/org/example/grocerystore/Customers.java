package org.example.grocerystore;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.grocerystore.common.model.Customer;
import org.example.grocerystore.server.MetricsServer;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

@Component
@RequiredArgsConstructor
@Slf4j
public class Customers {
    private final static AtomicLong CUSTOMER_COUNT = new AtomicLong(1);
    private final LinkedBlockingQueue<Customer> customerQueue = new LinkedBlockingQueue<>();
    private final AtomicInteger customerFreqAmount = new AtomicInteger(0);
    private final AtomicInteger customerFreqSeconds = new AtomicInteger(10);
    private final MetricsServer metricsServer;
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    /**
     * Start a loop that continuously adds customers to the customerQueue at a
     * configurable frequency
     */
    public void start(){
        scheduleAddCustomers();
    }

    private void scheduleAddCustomers(){
        executorService.schedule(() -> {
            addCustomers(customerFreqAmount.get());
            scheduleAddCustomers();
        }, customerFreqSeconds.get(), TimeUnit.SECONDS);
    }

    /**
     * Blocking
     * @return
     * @throws InterruptedException
     */
    public Customer next() throws InterruptedException {
        Customer customer = customerQueue.take();
        metricsServer.getWaitingCustomers().dec();
        return customer;
    }

    public void addCustomers(int amountToAdd){
        for (int i=0; i< amountToAdd; i++){
            newCustomer();
        }
    }

    private void newCustomer(){
        long customerNum = CUSTOMER_COUNT.getAndIncrement();
        String customerName = "customer-" + customerNum;
        metricsServer.getCustomerCount().set(customerNum);
        log.debug("Adding customer: {}", customerName);
        addCustomer(new Customer(customerName));
    }

    public void addCustomer(Customer customer){
        customerQueue.offer(customer);
        metricsServer.getWaitingCustomers().inc();
    }

    public void setCustomerFreqAmount(int amount){
        customerFreqAmount.set(amount);
    }
    public void setCustomerFreqSeconds(int seconds){
        customerFreqSeconds.set(seconds);
    }

}

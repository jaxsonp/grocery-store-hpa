package org.example.grocerystore;

import io.prometheus.client.Gauge;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.grocerystore.common.model.Clerk;
import org.example.grocerystore.common.model.Customer;
import org.example.grocerystore.server.MetricsServer;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

@Component
@RequiredArgsConstructor
@Slf4j
public class Clerks {
    private final LinkedBlockingQueue<Clerk> clerkQueue = new LinkedBlockingQueue<>();
    private final Customers customers;

    public void start(){
        Executors.newSingleThreadExecutor().execute(() -> {
            while(true){
                try {
                    Clerk clerk = clerkQueue.take();
                    Customer customer = customers.next();
                    sendCustomer(clerk, customer);
                } catch (InterruptedException ignored) {}
            }
        });
    }

    public void addClerk(Clerk clerk){
        clerkQueue.offer(clerk);
    }

    private void sendCustomer(Clerk clerk, Customer customer){
        log.debug("Sending customer {} to clerk {}", customer, clerk);
        WebClient webClient = WebClient.create("http://" + clerk.getBaseUrl());
        webClient.post()
                .uri("/clerk-callback/customer")
                .bodyValue(customer)
                .retrieve()
                .toBodilessEntity()
                .onErrorResume((e) -> {
                    log.warn("Could not send customer to clerk {}", clerk);
                    customers.addCustomer(customer);
                    return Mono.empty();
                })
                .subscribe();
    }

    public int getAvailableClerks(){
        return clerkQueue.size();
    }
}

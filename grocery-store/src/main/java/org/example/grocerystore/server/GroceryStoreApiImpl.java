package org.example.grocerystore.server;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.grocerystore.Clerks;
import org.example.grocerystore.Customers;
import org.example.grocerystore.common.api.GroceryStoreApi;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Slf4j
public class GroceryStoreApiImpl implements GroceryStoreApi {

    private final Customers customers;
    private final Clerks clerks;

    @Override
    public ResponseEntity<Void> open() {
        log.info("Opening...");
        clerks.start();
        customers.start();
        log.info("Open");
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> addCustomers(Integer num) {
        if (num == null || num < 0 || num > 1000){
            return ResponseEntity.badRequest().build();
        }
        customers.addCustomers(num);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> customerFreq(Integer amount, Integer seconds) {
        log.debug("Updating customer frequency");
        if (amount != null){
            if (amount < 0 || amount > 1000){
                log.warn("Invalid amount: {}", amount);
                return ResponseEntity.badRequest().build();
            }
            log.debug("Setting freq amount to {}", amount);
            customers.setCustomerFreqAmount(amount);
        }
        if (seconds != null){
            if (seconds < 0 || seconds > 60 * 10){
                log.warn("Invalid seconds: {}", seconds);
                return ResponseEntity.badRequest().build();
            }
            log.debug("Setting freq seconds to {}", seconds);
            customers.setCustomerFreqSeconds(seconds);
        }
        return ResponseEntity.ok().build();
    }

}

package org.example.grocerystore.server;

import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.Gauge;
import io.prometheus.client.Histogram;
import io.prometheus.client.exporter.common.TextFormat;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.StringWriter;
import java.io.Writer;

@RestController
@RequiredArgsConstructor
@Getter
@Slf4j
public class MetricsServer {

    private final Gauge customerCount = Gauge.build()
            .name("customer_count")
            .help("customer_count Total number of customers")
//            .labelNames("storeName")
            .register(CollectorRegistry.defaultRegistry);

    private final Gauge waitingCustomers = Gauge.build()
            .name("waiting_customers")
            .help("waiting_customers Number of waiting customers")
//            .labelNames("storeName")
            .register(CollectorRegistry.defaultRegistry);

    @GetMapping(value = "/metrics", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> getMetric(){
        Writer write = new StringWriter();
        try {
            TextFormat.write004(write, CollectorRegistry.defaultRegistry.metricFamilySamples());
        } catch (Exception ignored) {}
        return ResponseEntity.ok(write.toString());
    }
}

package org.example.grocerystore.server;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.grocerystore.Clerks;
import org.example.grocerystore.common.api.GroceryStoreClerkApi;
import org.example.grocerystore.common.model.Clerk;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Slf4j
public class GroceryStoreClerkApiImpl implements GroceryStoreClerkApi {

    private final Clerks clerks;

    @Override
    public ResponseEntity<Void> available(Clerk clerk) {
        log.debug("Adding clerk: {}", clerk);
        clerks.addClerk(clerk);
        return ResponseEntity.ok().build();
    }
}

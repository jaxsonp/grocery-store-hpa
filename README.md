# Overview
Example of configuring a Kubernetes Horizontal Pod Autoscaler (HPA) to auto-scale pods using external metrics in Prometheus.

To demonstrate auto-scaling, we will be using a grocery store scenario - grocery stores needs to increase or decrease the
number of checkout clerks to match the demand of customers. Too few clerks and the line gets backed up and customers
get angry. Too many clerks and the clerks are standing around doing nothing.

For this scenario, we will simulate customers waiting in queue to be checked out by a clerk. Customers will be added to 
the queue at a consistent rate, e.g. 4 customers per 10 seconds. We can also simulate a spike in customers by
adding a large number of customers all at once. Clerks can only checkout _one_ customer at a time and each clerk has
a checkout-rate - the amount of time it takes to checkout one customer, e.g. 10 seconds per customer. We will gather
metrics to track the efficiency of the store (metrics on customer rate, checkout rate, number of waiting customers) and
use those metrics to scale the number of clerks up or down to match demand.

The two components used to simulate this scenario are:
##### `grocery-store`
Adds customers to the queue at a configurable rate and offers the customers to available `grocery-store-clerks` for checkout.
The customer rate is configurable via HTTP API (commands are given below). This component also servers up 
two metrics to Prometheus: `customer_count` - a gauge metric that tracks the total
amount of customers that come through the store and `waiting_customers` - a gauge metric that tracks the 
number of customers waiting in the queue at any given time.
##### `grocery-store-clerk`
Checkouts out a single customer at a time at a configurable rate - configurable via `clerk.checkout-time-sec` property
(see application.yaml in `grocery-store-clerk`'s configmap of `kube/grocery-store.yml`). This is the component that will
be auto-scaled based on metrics queried by the HPA. This component provides the metric `checkout_seconds` - a histogram
metric that keeps track of the time is takes to checkout a customer.
# Quickstart
## Prerequisites
Assuming these are already installed 
- maven
- docker
- minikube
- helm

Add the prometheus-community helm charts repo
```shell
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
```
Create `monitoring` namespace
```shell
kubectl create ns monitoring
```

## Install
### Build grocery-store container images and deploy
Build container image and Load image into minikube
```shell
mvn clean install jib:dockerBuild
minikube image load grocery-store:999-SNAPSHOT
minikube image load grocery-store-clerk:999-SNAPSHOT
```

Deploy grocery-store
```shell
kubectl apply -f kube/grocery-store.yml
```

### Install Prometheus + Grafana (https://blog.marcnuri.com/prometheus-grafana-setup-minikube)
#### Prometheus
Install Prometheus via prometheus-community helm charts and expose the server
```shell
helm install --values kube/prometheus_values.yml -n monitoring prometheus prometheus-community/prometheus
kubectl expose service -n monitoring prometheus-server --type=NodePort --target-port=9090 --name=prometheus-server-np
```
Access the web-ui. Note: may have to wait a few minutes for the server to fully start
```shell
minikube service -n monitoring prometheus-server-np
```

#### Grafana (optional, prometheus UI may be sufficient)
Install Prometheus via grafana community helm charts
```shell
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
helm install -n monitoring grafana grafana/grafana
```

Expose the server
```shell
kubectl expose service -n monitoring grafana --type=NodePort --target-port=3000 --name=grafana-np
```

Get password
```shell
kubectl get secret -n monitoring grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
```

Access web-ui using `admin` and password retrieved from the previous command
```shell
minikube service -n monitoring grafana-np
```

### Install Prometheus Adaptor (https://github.com/prometheus-community/helm-charts/tree/main/charts/prometheus-adapter)
```shell
helm install --values kube/prometheus-adapter-values.yml -n monitoring prometheus-adapter prometheus-community/prometheus-adapter
```
Query kube metrics API for the external metrics that were configured. Should see the `customer_checkout_ratio`
metric in the response (may take a few minutes before the metrics show up)
```shell
kubectl get --raw /apis/external.metrics.k8s.io/v1beta1
```
## Operating
Now it's time to configure and "open" the `grocery-store` and see the `grocery-store-clerks` auto-scaling.

First, configure the frequency of customers
```shell
curl -X POST "$(minikube service -n grocery-store grocery-store --url)/store/customerFreq?amount=4&seconds=10"
```
Now "open" the `grocery-store` - this command will start adding customers, sending them to the available `grocery-store-clerks`
and start collecting metrics.
```shell
curl -X POST "$(minikube service -n grocery-store grocery-store --url)/store/open"
```
Hopefully, after ~30s or so, kubernetes will be auto-scaling the `grocery-store-clerk` pods to meet demand and the
number of pods should stabilize. For instance, if a single `grocery-store-clerk` has a checkout rate of 1 per 10s and the customer 
rate is 4 per 10s, there should be 4 (or maybe 5) `grocery-store-clerk` pods running.

To simulate a spike in customers and see how kubernetes responds, add 25 (or however many you'd like)
customers all at once with the command
```shell
curl -X POST "$(minikube service -n grocery-store grocery-store --url)/store/addCustomers?num=25"
```
The expected behavior would be for kubernetes to increase the number of pods (possibly to the max value) to handle all
the customers in line. And then, once the line has gone down, the number of pods should slowly decrease until it
stabilizes and the number of pods is back to the number it was before the spike. Tip: use the promql query `sum(waiting_customers)`
in the Prometheus UI to see the number of waiting customers change as pods are going up and down.
### Additional commands
minikube dashboard
```shell
minikube dashboard
```
Query the metrics in the `grocery-store`
```shell
curl "$(minikube service -n grocery-store grocery-store --url)/metrics"
```
Query kube metrics API for external metrics
```shell
kubectl get --raw /apis/external.metrics.k8s.io/v1beta1
```
Watch the `grocery-store-hpa` to see as the `target` and `replicas` change
```shell
watch -n 1 -t kubectl get hpa -n grocery-store grocery-store-hpa
```
Watch the `grocery-store` pods go up and down
```shell
watch -n 1 -t kubectl get pods -n grocery-store
```


### Useful PromQL Queries
R_current Current number of `grocery-store-clerk` pods
```
kube_deployment_spec_replicas{deployment="grocery-store-clerk"}
```

C_q Current number of waiting customers in the `grocery-store` queue
```
sum(waiting_customers)
```

C_in Current rate at which customers are being added to the `grocery-store` queue
```
sum(rate(customer_count{job="grocery-store"}[1m]))
```

C_out_instance Average checkout time per `grocery-store-clerk`
```
(sum(rate(checkout_seconds_count{job="grocery-store-clerk"}[1m])) / sum(rate(checkout_seconds_sum{job="grocery-store-clerk"}[1m])))
```

(C_q + (C_in/C_out_instance)) / R_current
```
(sum(waiting_customers) + (sum(rate(customer_count{job="grocery-store"}[1m]))/(sum(rate(checkout_seconds_count{job="grocery-store-clerk"}[1m])) / sum(rate(checkout_seconds_sum{job="grocery-store-clerk"}[1m]))))) / sum(kube_deployment_spec_replicas{deployment="grocery-store-clerk"})
```
